extern crate lstm;
use lstm::{
    gates::{ Forget, Input, Output },
    Signals
};

fn sig_gen(i: f32) -> f32 {
    if i % 30.0 != 0.0 {
        f32::sin(i)
    } else {
        0f32
    }
    
}

fn main() {
    let mut cells = vec![];
    for i in 0..4 {
        cells.push(lstm::Cell::default());
        dbg!(&cells[i]);
    }
    let x = sig_gen(0.0); 
    // for i in 0..200 {
        
    //     for j in 0..cells.len() - 1 {
    //         cells[j].signals(vec![x, 0.0, 0.0]);
    //         cells[j].forget(None);
    //         cells[j].input(None, None);
    //         let output = cells[j].output(None);
    //         cells[j + 1].signals(output.clone());
    //         cells[j].signals(vec![ sig_gen((i + 1) as f32), 0.0, 0.0]);
    //         println!("Cell# {:?} Signal: {:?} output: {:?}", j, x, output);
    //     }
        
    // }
}