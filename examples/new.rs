use lstm::{
    gates::{ Forget, Input, Output },
    Signals
};

fn train(num_features: usize) -> Vec<Cell> {
    let num_cells = 7; // Number of LSTM cells in the layer

    let mut cells: Vec<Cell> = Vec::with_capacity(num_cells);

    // Initialize the LSTM layer with 7 cells
    for _ in 0..num_cells {
        let cell = Cell::new();
        cells.push(cell);
    }

    // Train on the first num_features features of the sine wave
    for i in 0..num_features {
        let mut x: f64 = (i as f64).sin();
        let mut h = 0.0;
        let mut c = 0.0;

        let mut next_outputs: Vec<Vec<f64>> = Vec::new();

        // Process each cell in the LSTM layer
        for j in 0..num_cells {
            // Pass the current signal to the cell
            cells[j].signals(vec![x, h, c]);

            // Process the signal through the cell
            cells[j].input(None, None);
            cells[j].forget(None);
            next_outputs.push(cells[j].output(None));

            // Update hidden state for the next iteration
            x = next_outputs[j][0];
            h = next_outputs[j][1];
            c = next_outputs[j][2];
        }

        // Debugging output to check state_weight for each cell
        /*
        for j in 0..num_cells {
            println!("feature: {}, cell: {}, weight: {}", i, j, cells[j].state_weight);
        }
        */
    }
    
    // Return the trained LSTM layer
    cells
}





fn test(c: Vec<Cell>) {
    let mut cells = c.clone();
    // Test sine wave
    let num_predictions = 10; // Number of predictions to make
    let mut predictions: Vec<f64> = Vec::new();

    for _ in 0..num_predictions {
        // Get the last cell in the vector (should be the one with the latest signal)
        let mut cell = cells[cells.len() - 1].clone();

        // Calculate the next prediction
        let prediction = cell.output(None)[0]; // Assuming the first value in the output is the prediction

        // Store the prediction
        predictions.push(prediction);

        // Update the cells for the next time step (you can append the prediction to the input sequence)
        let mut next_signal = cell.signals.clone();
        next_signal[0] = prediction;
        let mut next_cell = cell.clone();
        next_cell.signals(next_signal);
        next_cell.input(None, None);
        next_cell.forget(None);
        next_cell.output(None);

        // Remove the first cell in the vector (it's no longer needed)
        cells.remove(0);

        // Add the updated cell to the end of the vector
        cells.push(next_cell);
    }

    // Print the predictions
    println!("Predictions: {:?}", predictions);
}

fn main() {
    let trained_lstm = train(100);
    dbg!(&trained_lstm);
}
