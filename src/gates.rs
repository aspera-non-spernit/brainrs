pub trait Forget { fn forget(&mut self, forget_bias: Option<f64>); }
pub trait Input { fn input(&mut self, input_bias: Option<f64>, state_bias: Option<f64>); }
pub trait Output { fn output(&mut self, output_bias: Option<f64>) -> Vec<f64>; }