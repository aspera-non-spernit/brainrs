pub trait Activation {
    fn sigmoid(&self, x: f64) -> f64;
    fn tanh(&self, x: f64) -> f64;
    fn relu(&self, x: f64) -> f64;
}