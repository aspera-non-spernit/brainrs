//! A library for Long-Term-Short-Term memory
extern crate rand;
#[macro_use] extern crate serde;
extern crate ndarray;
pub mod activations;
pub mod gates;
use activations::Activation;
use gates::{ Forget, Input, Output };
use ndarray::{ arr1 };
use rand::Rng;
use std::fmt;

// https://colah.github.io/posts/2015-08-Understanding-LSTMs/

pub trait Signals { fn signals(&mut self, signals: Vec<f64>); }

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Cell {
    pub signals: Vec<f64>,      // holds current signal, previous hidden state and previous state
    forget_weight: f64,
    forget_bias: f64, 
    forget_signal: f64,
    input_weight: f64,
    input_bias: f64,
    state_weight: f64,
    state_bias: f64,
    state_signal: f64,
    output_weight: f64,
    output_bias: f64
}
    
impl Cell {
    pub fn new() -> Self {
        let mut rng = rand::thread_rng();
        Cell {
            signals: vec![0.0, 0.0, 0.0], // x, h-1, c-1
            forget_weight: rng.gen_range(0.0..1.0),
            forget_bias: rng.gen_range(0.0..1.0),
            forget_signal: rng.gen_range(0.0..1.0),
            input_weight: rng.gen_range(0.0..1.0),
            input_bias: rng.gen_range(0.0..1.0),
            state_weight: rng.gen_range(0.0..1.0),
            state_bias: rng.gen_range(0.0..1.0),
            state_signal: rng.gen_range(0.0..1.0),
            output_weight: rng.gen_range(0.0..1.0),
            output_bias: rng.gen_range(0.0..1.0),
        }
    }
}
    


impl Default for Cell {
    fn default() -> Self {
        let mut rng = rand::thread_rng();
        Cell { 
            signals: vec![0.0, 0.0, 0.0], // x, h-1, c-1
            forget_weight: rng.gen_range(0.0..1.0),
            forget_bias: rng.gen_range(0.0..1.0),
            forget_signal: rng.gen_range(0.0..1.0),
            input_weight: rng.gen_range(0.0..1.0),
            input_bias: rng.gen_range(0.0..1.0),
            state_weight: rng.gen_range(0.0..1.0),
            state_bias: rng.gen_range(0.0..1.0),
            state_signal: rng.gen_range(0.0..1.0),
            output_weight: rng.gen_range(0.0..1.0),
            output_bias: rng.gen_range(0.0..1.0),
        }
    }
}


impl Signals for Cell {
    fn signals(&mut self, signals: Vec<f64>) {
        self.signals = signals;
    }
}

impl Forget for Cell {
    fn forget(&mut self, forget_bias: Option<f64>) {
        if let Some(fb) = forget_bias { self.forget_bias = fb; }
        let weighted_signals = self.forget_weight * arr1(&[self.signals[0], self.signals[1]] ); 
        self.forget_signal = self.sigmoid( weighted_signals[0] + weighted_signals[1] + self.forget_bias);
    }
}

impl Input for Cell {
    fn input(&mut self, input_bias: Option<f64>, state_bias: Option<f64>) {
        let sig_arr = arr1(&[self.signals[0], self.signals[1]] );
        if let Some(ib) = input_bias { self.input_bias = ib; }
        if let Some(sb) = state_bias { self.state_bias = sb; }
        let mut weighted_signals = self.input_weight * &sig_arr; 
        let it = self.sigmoid ( weighted_signals[0] + weighted_signals[1] + self.input_bias );
        weighted_signals = self.state_weight * sig_arr;
        let state_candidate = f64::tanh(weighted_signals[0] + weighted_signals[1] + self.state_bias);
        self.state_signal = self.forget_signal * self.signals[2] + it * state_candidate;
        println!("State Signal: {}", self.state_signal);

    }
}

/**
* ChatGPT says: Due to the single weights in this cell, the hidden state is implicitly calculated in the output gate.
* Therefore there's no need to calculaute the hidden state as the output gate implicitly carries the hidden value to the next cell.
**/

impl Output for Cell {
    fn output(&mut self, output_bias: Option<f64>) -> Vec<f64> {
        let sig_arr = arr1(&[self.signals[0], self.signals[1]]);
        if let Some(ob) = output_bias {
            self.output_bias = ob;
        }
        let weighted_signals = self.output_weight * &sig_arr;
        let output_candidate = f64::tanh(weighted_signals[0] + weighted_signals[1] + self.output_bias);
        let ot = output_candidate * f64::tanh(self.state_signal);

        // Print the hidden state
        println!("Hidden State: {}", self.state_signal);

        vec![ot, ot, self.state_signal]
    }
}

impl Activation for Cell {
    fn sigmoid(&self, x: f64) -> f64 {
        1.0 / (1.0 + std::f64::consts::E.powf( -x) ) // ChatGPT: 1.0 / (1.0 + (-x).exp())
    }

    fn tanh(&self, x: f64) -> f64 {
        x.tanh()
    }

    fn relu(&self, x: f64) -> f64 {
        if x > 0.0 {
            x
        } else {
            0.0
        }
    }

}