
[![Build Status](https://travis-ci.org/aspera-non-spernit/brainrs.svg?branch=master)](https://travis-ci.org/aspera-non-spernit/brainrs)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
![Rust Version](https://img.shields.io/badge/rust-stable%201.22.1%20(05e2e1c41%202017--12--22)-lightgrey.svg)

# lstm